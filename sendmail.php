<?php
// Email Submit
// Note: filter_var() requires PHP >= 5.2.0
if($_POST)
{
    $to_email       = "hola@ciudadqueviste.com"; //Recipient email, Replace with own email here
    
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        
        $output = json_encode(array( //create JSON data
            'type'=>'error', 
            'text' => 'Sorry Request must be Ajax POST'
        ));
        
        die($output); //exit script outputting json data
    } 
    
    if ( isset($_POST['email']) && isset($_POST['name']) && isset($_POST['message']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {

        // detect & prevent header injections
        $test = "/(content-type|bcc:|cc:|to:)/i";
        foreach ( $_POST as $key => $val ) {
        if ( preg_match( $test, $val ) ) {
          exit;
        }
        }
        
        //Sanitize input data using PHP filter_var().
        $user_name      = $_POST['name'];
        $user_email     = $_POST['email'];
        $subject        = 'Mensaje de marca';
        $message        = filter_var($_POST["message"], FILTER_SANITIZE_STRING);
        
        //email body
        $message_body = $message."\r\n\r\n-".$user_name."\r\nEmail : ".$user_email ;
        
        //proceed with PHP email.
        $headers = 'From: '.$user_email.'' . "\r\n" .
        'Reply-To: '.$user_email.'' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to_email, $subject, $message_body, $headers);

        //
//        mail( "hola@ciudadqueviste.com", WAVA , $_POST['message'], "From:" . $_POST['email'] );
        //			^
        //  Replace with your email
    }
}
?>